import {createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw} from 'vue-router';
import Login from '../views/Login.vue';
import Index from '../views/Index.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/mec',
    name: 'Mec',
    component: () => import ('../views/mec.vue')
  },
  {
    path: '/test',
    name: 'Test',
    component: () => import('../views/test.vue'),
  },
  {
    path: '/index',
    name: 'Index',
    component: Index,
    redirect: '/home',
    children: [
      // {
      //   path: 'home',
      //   name: 'Home',
      //   component: () => import('../views/mainPage/navHome.vue')
      // },
      {
        path: '/home',//子组件的路径前加'/'则默认为根路由，url上就不显示父组件的path，即/home。如果不加'/'，则会显示父组件的path，如/index/home
        name: 'Home',
        meta: {
          keepAlive: true,
        },
        component: () => {
          return import('../views/mainPage/navHome.vue')
          // if(false){
          //   return import('../views/mainPage/hpe/navHome.vue')
          // }else{
          //   return import('../views/mainPage/navHome.vue')
          // }
        },
      },
      {
        path: '/allContents',
        name: 'AllContents',
        component: () => import('../views/mainPage/navAllContents.vue')
      },
      {
        path: '/tracks',
        name: 'Tracks',
        component: () => import('../views/mainPage/navTracks.vue')
      },
      {
        path: '/content',
        name: 'Content',
        component: () => import('../views/mainPage/navContent.vue')
      },
      {
        path: '/contributors',
        name: 'Contributors',
        component: () => import('../views/mainPage/navContributors.vue')
      },
      {
        path: '/presentation',
        name: 'Presentation',
        component: () => import('../views/mainPage/navPresentation.vue')
      },
      {
        path: '/notification',
        name: 'Notification',
        component: () => import('../views/mainPage/navNotification.vue')
      },
      {
        path: '/favorites',
        name: 'Favorites',
        meta: {
          keepAlive: true,
        },
        component: () => import('../views/mainPage/navFavorites.vue')
      },
      {
        path: '/agenda',
        name: 'Agenda',
        meta: {
          keepAlive: true,
        },
        component: () => import('../views/mainPage/Agenda.vue')
      },
      {
        path: '/searchAgenda',
        name: 'searchAgenda',
        meta: {
          keepAlive: true,
        },
        component: () => import('../views/mainPage/searchAgenda.vue')
      },
    ]
  }
];


const router = createRouter({
  history: createWebHashHistory(),
  // history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
