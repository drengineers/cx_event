import axios from 'axios';

axios.interceptors.request.use((config) => {
    // 一般在这个位置判断token是否存在
    // let token = "FB76TXI2DcLQet8oBYTgrn2W";
    let token = localStorage.getItem("webToken");
    config.headers.CX_TOKEN = token;
    return config;
}, (error) => {
    console.log("ERROR:", error)
    // 对请求错误做些什么
    return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
    // 处理响应数据
    if (response.status === 200) {
        return Promise.resolve(response);
    } else {
        return Promise.reject(response);
    }
}, (error) => {
    console.log("ERROR:", error)
    // 处理响应失败
    return Promise.reject(error);
});

export default axios

