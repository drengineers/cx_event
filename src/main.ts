import {createApp} from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/css/global.css';
import './assets/iconfont/iconfont.css';
import './assets/css/vmware.scss';
import swiper from '@/plugins/swiper'

const app = createApp(App);
app.use(store).use(router).use(ElementPlus).use(swiper).mount('#app');

// router.beforeEach((to, from, next) => {
//   // 这里判断用户是否登录，验证本地存储是否有token
//   if (!localStorage.webToken) { // 判断当前的token是否存在
//     //token不存在
//     if (to.query.token) {
//       next();
//     } else {
//       console.log("==== =====",2222222222)
//       next();
//
//       // next({
//       //   path: '/login',
//       //   // query: {redirect: to.fullPath}
//       // });
//     }
//
//     // next()
//   } else {
//     //token存在
//     console.log('====to.path =====',to.path);
//     if(to.path=='/login'){
//       localStorage.setItem("webToken","");
//     }
//     next();
//   }
// });

router.beforeEach((to, from, next) => {
  // console.log("====qurey =====",to)
  if (localStorage.webToken) {
    if (to.path == '/login') {
      localStorage.setItem('webToken', '');
      next();
    } else if (to.path === null) {
      console.log("==== =====",22222222)
      next('*');
    } else {
      next();
    }
  } else {
    if (to.query.token) {
      if(process.env.NODE_ENV === 'production'){
        next();
      }else{
        localStorage.setItem("webToken",JSON.stringify(to.query.token));
        next('/mec');
      }
    }else{
      if (to.path == '/login') {
        next();
      }else{
        next('/login');
      }
    }
    // if (to.path == '/login') {
    //   next();
    // }else{
    //   next('/login');
    // }
  }

});

// router.beforeEach((to, from, next) => {
//   console.log("====token =====",to.query.token)
//   next();
//   if (localStorage.webToken) {
//     console.log("11111111",)
//     // if (to.path == '/login') {
//     //   localStorage.setItem('webToken', '');
//     //   next();
//     // } else if (to.path === null) {
//     //   next('*');
//     // } else {
//     //   next();
//     // }
//     next();
//   } else {
//     console.log("222222",)
//     console.log("====token =====",to.query.token)
//     if(to.query.token){
//       console.log(3333333)
//       next('/home');
//     }else{
//       next();
//     }
//     // if (to.query.token) {
//     //   // localStorage.setItem("webToken",to.query.token);
//     //   next('/mec');
//     // }else{
//     //   if (to.path == '/login') {
//     //     next();
//     //   }else{
//     //     next('/login');
//     //   }
//     // }
//   }
// });
