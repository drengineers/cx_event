import { createStore } from 'vuex'
import appStore from './navigation/index'
import searchAgenda from './searchAgenda/index'
import searchBox from './searchBox/index'

export default createStore({
  state: {

  },
  getters: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    appStore,
    searchAgenda,
    searchBox
  }
})
