module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/officialWebsite/' : '/',
    devServer: {
        open: false,//为true时，npm run serve 自动打开浏览器
        host: "localhost",
        port: 8081,
        https: false,
        proxy: {
            "/vmWare": {
                // target: "https://cx-app-vmware.uc.r.appspot.com/vmWare",
                target: "https://7-1-2-dot-cx-app-vmware.uc.r.appspot.com/vmWare",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/vmWare": ""
                }
            },
            "/m": {
                // target: "https://cx-app-vmware.uc.r.appspot.com/m",
                target: "https://7-1-2-dot-cx-app-vmware.uc.r.appspot.com/m",
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/m": ""
                }
            }
        },

    },
};
